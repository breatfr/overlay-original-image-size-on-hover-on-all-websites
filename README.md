# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>
Add an overlay with image size on all images on web.
## Preview
![Preview](https://gitlab.com/breatfr/overlay-original-image-size-on-hover-on-all-websites/-/raw/main/docs/preview.jpg)

## How to use in few steps
1. Install Violentmonkey browser extension
    - Chromium based browsers link: https://chromewebstore.google.com/detail/violentmonkey/jinjaccalgkegednnccohejagnlnfdag
        - Brave
        - Chromium
        - Google Chrome
        - Iridium Browser
        - Microsoft Edge
        - Opera
        - Opera GX
        - SRWare Iron
        - Ungoogled Chromium
        - Vivaldi
        - Yandex Browser
        - many more
    - Firefox based browsers link: https://addons.mozilla.org/fr/firefox/addon/violentmonkey/
        - Mozilla Firefox
        - Mullvad Browser
        - Tor Browser
        - Waterfox
        - many more

2. Go on [Greasy Fork](https://greasyfork.org/scripts/497851) website and click on `Install` or open the [GitLab version](https://gitlab.com/breatfr/overlay-original-image-size-on-hover-on-all-websites/-/raw/main/js/overlay-original-image-size-on-hover-on-all-websites.user.js).

3. To update the script, open the `Violentmonkey settings` window and click on `Check for update`

4. Enjoy :)
# [![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/breatfr) <a href="https://www.paypal.me/breat"><img src="https://github.com/andreostrovsky/donate-with-paypal/raw/master/blue.svg" alt="PayPal" height="30"></a>